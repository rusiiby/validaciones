package com.example.Validadciones.model.dao;

import com.example.Validadciones.model.entity.Empresa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IEmpresaDao extends PagingAndSortingRepository<Empresa,Long> {

    public Empresa findByNombreAndRuc(String id,Integer ruc);

    public Page<Empresa> findAll(Pageable page);

}
