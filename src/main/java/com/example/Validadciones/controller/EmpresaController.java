package com.example.Validadciones.controller;


import com.example.Validadciones.model.entity.Empresa;
import com.example.Validadciones.model.service.IEmpresaService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class EmpresaController {


    @Autowired
    private IEmpresaService empresaService;

    Logger logger = LoggerFactory.getLogger(EmpresaController.class);

    @GetMapping("/empresa/ListPage")
    public ResponseEntity<?> listPage(@RequestParam Integer pageNumber, @RequestParam Integer pageSize){

        logger.info("===============INICIO DEL METODO LISTAR PAGE=============");
        Map<String,Object> response = new HashMap<>();
        Pageable page = PageRequest.of(pageNumber, pageSize);
        response.put("data", empresaService.findALl(page));
        logger.info("============FIN DEL METODO LISTAR  PAGE ==========");
        return   new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
    }


    @PostMapping("/empresa/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@Valid @RequestBody Empresa empresa, BindingResult buiBindingResult){

        Map<String,Object> response = new HashMap<>();

        if(buiBindingResult.hasErrors()){
            List<String> errors = new ArrayList<>();
            for (FieldError err : buiBindingResult.getFieldErrors()){
                errors.add(err.getField()+" "+ err.getDefaultMessage());
            }
            response.put("errores",errors);
            return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
        }
        try {
            empresaService.save(empresa);
            response.put("message","success");
            response.put("code",HttpStatus.CREATED.value());

        }catch (IllegalArgumentException e){
            response.put("message", "Error Controlado");
            response.put("error", e.getMessage().concat(":"));

        }

        catch (DataAccessException e){
            logger.warn("Excepcion en la bbdd");
            response.put("message", "Error al realizar insert en la BBDD");
            response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));

            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }catch (Exception e){

            response.put("message", "Error al realizar insert en la BBDD");
            response.put("error", e.getMessage().concat(":").concat(e.getLocalizedMessage()));
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response,HttpStatus.CREATED);
    }
}
