package com.example.Validadciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidadcionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidadcionesApplication.class, args);
	}

}
